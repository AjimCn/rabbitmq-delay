package com.lagou.order.service;

import com.lagou.order.domain.Order;

public interface OrderService {
    /**
     *
     * @param order 商品
     * @return
     */
    Long takeOrder(Order order);

    Integer payOrder(Long orderId);
}
