package com.lagou.order.domain;


import lombok.Data;

@Data
public class Order {
    private Long id;

    private String name;

    private Integer status;
}