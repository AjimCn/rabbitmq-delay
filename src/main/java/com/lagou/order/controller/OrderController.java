package com.lagou.order.controller;

import com.lagou.order.domain.Order;
import com.lagou.order.service.OrderService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("order")
public class OrderController {

    @Resource
    private OrderService orderService;

    @GetMapping("takeOrder")
    public  String takeOrder(Order order){
        order.setStatus(1);
        Long aLong = orderService.takeOrder(order);
        return aLong + "下单成功";
    }

    @GetMapping("payOrder")
    public  String payOrder(Long orderId){
        Integer integer = orderService.payOrder(orderId);
        if(integer == 1){
            return "付款成功";
        }
        return "付款失败";

    }
}
