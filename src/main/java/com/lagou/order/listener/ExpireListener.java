package com.lagou.order.listener;

import com.lagou.order.domain.Order;
import com.lagou.order.mapper.OrderMapper;
import com.rabbitmq.client.Channel;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

@Service
@Transactional
public class ExpireListener {

    @Resource
    private OrderMapper orderMapper;

    @RabbitListener(queues = "queue.expired")
    public void onMessage(Message message, Channel channel) throws  Exception{
        // 接收消息
        String idStr = new String(message.getBody(), message.getMessageProperties().getContentEncoding());
        //从消息里面拿到订单号
        Long id = Long.valueOf(idStr);
        //查询订单状态
        Order order = orderMapper.selectByPrimaryKey(id);
        //如果订单状态为正常,标记为过期
        Integer status = order.getStatus();
        if(status == 1){
            order.setStatus(3);
            orderMapper.updateByPrimaryKey(order);
        }
        //如果订单状态为已经支付,就不用进行处理
        channel.basicAck(message.getMessageProperties().getDeliveryTag(),false);
    }
}
